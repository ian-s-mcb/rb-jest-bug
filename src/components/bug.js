import React from 'react'

import { Nav, NavDropdown } from 'react-bootstrap'

const Bug = () => (
  <div className="App">
    <Nav>
      <NavDropdown title="Dropdown" id="collapsible-nav-dropdown">
        <NavDropdown.Item>
          A nav dropdown item
        </NavDropdown.Item>
      </NavDropdown>
    </Nav>
  </div>
)

export default Bug
