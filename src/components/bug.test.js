import React from 'react';
import ReactDOM from 'react-dom';
import renderer from "react-test-renderer"

import Bug from './bug';

it("renders correctly", () => {
  const tree = renderer
    .create(
      <Bug />
     )
    .toJSON()
  expect(tree).toMatchSnapshot()
})
