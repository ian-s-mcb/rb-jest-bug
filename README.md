# rb-jest-bug

## Test library versions
```bash
$ yarn list --pattern babel-jest jest react-test-renderer
yarn list v1.15.2
warning Filtering by arguments is deprecated. Please use the pattern option instead.
├─ babel-jest@24.8.0
├─ jest@24.8.0
└─ react-test-renderer@16.8.6
```

## Steps to reproduce bug 
* In step-8, the files contents of the 5 files came from this source:
* https://www.gatsbyjs.org/docs/unit-testing/

```bash
 1  gatsby new rb-jest-bug https://github.com/gatsbyjs/gatsby-starter-default.git
 2  cd rb-jest-bug/
 3  yarn
 4  yarn add -D jest babel-jest react-test-renderer babel-preset-gatsby identity-obj-proxy
 5  git commit -am "Install test libraries"
 6  mkdir __mocks__/
 7  touch jest-preprocess.js jest.config.js loadershim.js __mocks__/file-mock.js __mocks__/gatsby.js
 8  vim jest-preprocess.js jest.config.js loadershim.js __mocks__/file-mock.js __mocks__/gatsby.js
 9  git add jest-preprocess.js jest.config.js loadershim.js __mocks__/
10  git commit -m "Add Gatsby specific Jest config"
11  yarn add react-bootstrap
12  git commit -am "Install react-bootstrap"
13  touch src/components/bug{,.test}.js
14  vim src/components/bug{,.test}.js
15  vim package.json
16  git add package.json
17  git commit -m "Add test script to package.json"
18  git add src/components/bug.*
19  git commit -m "Add bug component, bug test"
20  yarn test
```
